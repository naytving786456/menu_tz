from django import template
from .models import MenuItem
from django.urls import reverse

register = template.Library()



@register.simple_tag(takes_context=True)
def draw_menu(context, menu_name):
    menu_items = MenuItem.objects.filter(menu=menu_name, parent=None).select_related('children')

    def render_menu_items(menu_items):
        result = ''
        for item in menu_items:
            if item.url:  # Проверяем, задан ли URL явно
                url = item.url
            else:
                url = reverse(item.url_name)  # Используем named URL, если явный URL не задан
            result += f'<li><a href="{url}">{item.title}</a>'
            if item.children.all():  # Связанные объекты уже были предварительно загружены, так что нет дополнительных запросов
                result += '<ul>'
                result += render_menu_items(item.children.all())
                result += '</ul>'
            result += '</li>'
        return result

    return render_menu_items(menu_items)
